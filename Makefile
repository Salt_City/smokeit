OUTPUT_DIR 		= target/k8s
ENVIRONMENT     = dev
BUILD_VERSION   = "SNAPSHOT"
IMAGE_NAME 		= mobile/flagger-smokeit
IMAGE_TAG 		= $(BUILD_VERSION)

build-executable: cleanup
ifdef RELEASE
	@echo "Building release executable"
	@cargo build --release
else
	@echo "Building executable"
	@cargo build
endif

build-image:
	@echo "Building image $(IMAGE_NAME):$(IMAGE_TAG)..."
	@docker build --pull --tag $(IMAGE_NAME):$(IMAGE_TAG) .
	@docker save $(IMAGE_NAME):$(IMAGE_TAG) > target/image.tar

release-image:
	@.azure/release-image.sh $(IMAGE_NAME) $(IMAGE_TAG)

build-k8s: cleanup
	@echo "Building k8s configuration yaml's..."
	@./k8s/build.sh $(IMAGE_TAG) $(ENVIRONMENT)

deploy-k8s: build-k8s
	@kubectl apply -f $(OUTPUT_DIR)/smokeit.yaml

destroy-k8s: build-k8s
	@kubectl delete -f $(OUTPUT_DIR)/smokeit.yaml || true

cleanup:
	@echo "Cleanup target directory..."
	@rm -rf target/*
