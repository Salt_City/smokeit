#!/usr/bin/env sh
set -o errexit
set -o nounset

# Directories.
SOURCE_DIR="$(dirname $0)"
OUTPUT_DIR=${OUTPUT_DIR:-$(pwd)/target/k8s}

# Binaries
ENVSUBST=${ENVSUBST:-envsubst}
command -v "${ENVSUBST}" >/dev/null 2>&1 || echo -v "Cannot find ${ENVSUBST} in path."

# Parameters
export IMAGE_TAG="$1"
export ENVIRONMENT="$2"

mkdir -p "${OUTPUT_DIR}"

kubectl kustomize "${SOURCE_DIR}/environments/${ENVIRONMENT}" | envsubst > "${OUTPUT_DIR}/smokeit.yaml"