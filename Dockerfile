FROM ghcr.io/fluxcd/flagger-loadtester:0.18.0 AS flagger-env

FROM debian:bullseye-slim

USER root

COPY target/release/smokeit /usr/local/bin/smokeit
COPY scripts/smokeit-pack /usr/local/bin/smokeit-pack
RUN chmod +x /usr/local/bin/smoke*

# USER app
#### flagger loadtester config
RUN groupadd --system app && \
    adduser --system -group app && \
    apt-get clean && \
    apt-get update && \
    apt-get -qy install ca-certificates curl jq libgcc-9-dev wget unzip

WORKDIR /home/app

COPY --from=flagger-env /opt/bats/ /opt/bats/
RUN ln -s /opt/bats/bin/bats /usr/local/bin/

COPY --from=flagger-env /usr/local/bin/hey /usr/local/bin/
COPY --from=flagger-env /usr/local/bin/helm /usr/local/bin/
COPY --from=flagger-env /usr/local/bin/tiller /usr/local/bin/
COPY --from=flagger-env /usr/local/bin/ghz /usr/local/bin/
COPY --from=flagger-env /usr/local/bin/helmv3 /usr/local/bin/
COPY --from=flagger-env /usr/local/bin/grpc_health_probe /usr/local/bin/
COPY --from=flagger-env /tmp/helm-tiller /tmp/helm-tiller
COPY --from=flagger-env /home/app/loadtester .
ADD https://raw.githubusercontent.com/grpc/grpc-proto/master/grpc/health/v1/health.proto /tmp/ghz/health.proto

RUN chown -R app:app ./
RUN chown -R app:app /tmp/ghz

USER app

# test load generator tools
RUN hey -n 1 -c 1 https://flagger.app > /dev/null && echo $? | grep 0
# RUN wrk -d 1s -c 1 -t 1 https://flagger.app > /dev/null && echo $? | grep 0

# install Helm v2 plugins
# RUN helm init --client-only && helm plugin install /tmp/helm-tiller

ENTRYPOINT ["./loadtester"]
