# SmokeIT![smoke wisp](./.docs/resources/smoke_2.png)
**SmokeIT** is a tool for running *integration*, *load* or *behavior driven development (bdd)* test scenarios against OpenAPI services.

## Installation

TODO...

## Usage

```shell
smokeit [FLAGS] [OPTIONS] <INPUT> [SUBCOMMAND]
```
#### FLAGS
    -h, --help       Prints help information
    -V, --version    Prints version information
    -v               level of verbosity

#### OPTIONS
    -e, --env <ENV>            environment to test against
    -o, --openapi <FILE>       OpenAPI file to validate against
    -p, --profile <PROFILE>    aws profile to use
    -r, --rate <RATE>          rate a user should hit the service
    -t, --time <TIME>          amount of time to run the load tests
    -u, --url <URL>            base url to send requests to
    -U, --users <USERS>        number of users to simulate

#### ARGS
    <INPUT>    Scenario file to test with

#### SUBCOMMAND
    generate       Generate scenarios for the supplied openapi file - NOTE : Not available at this time
    integration    Run integration tests
    load           Run load tests
    bdd            Run Behavior Driven Development (BDD) tests - NOTE : Not available at this time

## Commands

### Generate

Generate (or update) scenarios for the supplied openapi file.

```shell
smokeit scenarios.yaml generate --openapi openapi.yaml 
```

### Integration

Run integration tests for the supplied scenarios file.

```shell
smokeit scenarios.yaml integration --url https://some.url 
```

Run integration tests and validate against an openapi file.
```shell
smokeit scenarios.yaml integration --url https://some.url --openapi openapi.yaml 
```

### Load

Run load tests for the supplied scenarios file.

```shell
smokeit scenarios.yaml load --url https://some.url --users 64 --rate 100 --time 2m
```
- `--users 64` will launch the load of 64 users simultaneously.
  Each user uses an independent thread which uses memory, you may start consuming a lot with more than 1000 users.
- `--rate 100` will limit the request per second to this service to 100 per second.
  This is across all the users and all the paths. Total *maximum* the server will see.
  Note that if the amount of users is not enough, the server may see less than 100.
  Note that with short runs (< `1m`) the time to shutdown all threads may cause a lower total average of request per second.
- `--time 2m` Test for 2 minutes.
  You can use other such notations like 90s or 1h30m , etc.

### Behavior Driven Development (BDD)

Run bdd tests for the supplied scenarios file.

```shell
smokeit scenarios.yaml bdd --url https://some.url

# Can also run bdd tests under load.
smokeit scenarios.yaml bdd --url https://some.url --users 64 --rate 100 --time 2m
```

## Scenarios

Scenarios are defined in a yaml file and broken up into three sections ([data](#data), [test](#test), [bdd](#bdd)).
Soksemo will use the scenario file to create and execute requests against api services.

### Intrinsic Functions

Soksemo provides several built-in functions that help manage the scenarios. Use intrinsic functions in your templates to assign values to properties that are not available until runtime. 

- `$Ref` - returns the value of the specified parameter or resource 
  - example: `$Ref scenarioName`
  - Parameter
    - Needs to be in quotes
    - Starts with `scenarioName`
    - To retrieve child objects of resource use dot notation
      - `scenarioName.some.value`
      - When resource is an array of objects - Note : this functionality is not available at this time
        - `scenarioName.some.value`
            - return array of `value`
        - `scenarioName.some[].value`
            - return random `value`
        - `scenarioName.some[0].value`
            - return `value` at index 0
- `$Encrypt` - encrypts the string 
  - example: `$Encrypt $Ref scenarioName`
- `$Decrypt` - decrypt the string
    - example: `$Encrypt $Ref scenarioName`
- `$Base64` - returns the Base64 representation of the input string 
  - example: `$Base64 $Ref scenarioName`

### Schema

The [data](#data) and [test](#test) sections use the same schema layout to define scenarios.

```yaml
  - name: "exampleName"
    path: [https://optional.url]/some/path[/{}]
    pause: # Optional - number of seconds to pause before executing the request
    [get/post/put/delete]: # If `DATA` object, then only `get/post` are supported
      params: # Optional - HashMap of parameters in "String: Value" pairs
      headers: # Optional - HashMap of headers in "String: Value" pairs
      body: [{}] # Optional - Body of request, can be an array of objects, use `post` instead of `get`
      scenarios: # Optional - if not used, will make the request with the global parameters, expects a 2## status and store the response using "exampleName"
        - name: "scenarioName" # Name of scenario
          params:  # Optional - HashMap of parameters in "String: Value" pairs
          headers:  # Optional - HashMap of headers in "String: Value" pairs
          body: [{}] # Optional - Body of request, can be an array of objects, use `post` instead of `get`
          response:
            status: # Optional - defaults to 2## - Expected HTTP Status
            store: "true/false" # Optional - defaults to `true` if status 2## - used to store the response data for later tests, reference using "exampleName::scenarioName"
```

### Data

The data section is used to gather data from different services to be used as parameters to the test scenarios.
The data is stored and can be retrieved by other scenarios using the [!Ref](#intrinsic-functions) intrinsic function. 

**example:** Retrieve *active* and *inactive* users to be used for later test scenarios
```yaml
data:
  - name: "users"
    path: /users
    get: 
      scenarios:
        - name: "active"
          params:
            status: "active"
          headers:
            page: 0
            size: 25
        - name: "inactive"
          parms:
            status: "inactive"
```



### Test

The test section is used to define the test scenarios to be executed.

**example:** Test the `/accounts` path with *active* and *inactive* users retrieved in the data section.
```yaml
test:
  - name: "accounts"
    path: /accounts
    get:
      params:
        userId: [1, 2, 3] # Randomly test with one of these values
      scenarios:
        - name: "success" # Tests with global userId parameters expecting status 2##
        - name: "active" # Tests with active user id's expecting status 2##
          params: # Parameters here override global parameters
            userId: $Ref active::id # Randomly test with an active user id
        - name: "inactive"
          params: # Parameters here override global parameters
            userId: $Ref inactive::id # Randomly test with an inactive user id
          response:
            status: 400
```

### BDD

The Behavior Driven Development (BDD) section is used to define scenarios that simulate a users' interaction with services.
This works by defining tests in the [test](#test) section and assigning an order of execution in the bdd section.

```yaml
bdd:
  - test_account_lookup:
    - user: # Test Name
      - login # Scenario Name
    - account: # Test Name
      - find_by_user # Scenario Name
  - test_account_change:
    - user:
      - login
    - account:
      - find_by_user
      - update_account_name
```

