#!/usr/bin/env sh
set -o errexit
set -o nounset

IMAGE_NAME="$1"
IMAGE_TAG="$2"

echo "$MAVEN_REPO_PASS" | docker login -u $MAVEN_REPO_USER --password-stdin $DOCKER_MOBILE_REGISTRY
docker image tag "$IMAGE_NAME:$IMAGE_TAG" "$DOCKER_MOBILE_REGISTRY/$IMAGE_NAME:$IMAGE_TAG"

echo "Releasing image $DOCKER_MOBILE_REGISTRY/$IMAGE_NAME:$IMAGE_TAG ..."
docker push "$DOCKER_MOBILE_REGISTRY/$IMAGE_NAME:$IMAGE_TAG"

if [[ "$IMAGE_TAG" != *"SNAPSHOT"* ]]
then
  docker image tag "$IMAGE_NAME:$IMAGE_TAG" "$DOCKER_MOBILE_REGISTRY/$IMAGE_NAME:latest"

  echo "Releasing image $DOCKER_MOBILE_REGISTRY/$IMAGE_NAME:latest ..."
  docker push "$DOCKER_MOBILE_REGISTRY/$IMAGE_NAME:latest"
fi
