use clap::ArgMatches;
use openapiv3::{OpenAPI, Operation};
use crate::model::schema::*;
use crate::model::goose::*;
use openapi_utils::{SpecExt, ReferenceOrExt};
use rand::Rng;
use serde_yaml::Value;
use colored::*;
use serde::de::DeserializeOwned;
use goose::prelude::*;
use goose::config::GooseConfiguration;
use std::str::FromStr;
use std::sync::RwLock;
use std::collections::{HashMap, HashSet};
use lazy_static::lazy_static;

static INTEGRATION: &str = "integration";
static LOAD_TEST: &str = "load";
static DRY_RUN: &str = "dry"; // todo - probably convert this to the generate command
static INPUT: &str = "INPUT";
static OPEN_API: &str = "openapi";
static DATA: &str = "data";

lazy_static! {
    static ref LOAD_TEST_SCENARIOS: RwLock<Vec<GooseScenario>> =
        RwLock::new(Vec::new());
    pub static ref RETURN_DATA: RwLock<HashMap<String, Value>> =
        RwLock::new(HashMap::new());
}

pub async fn command(matches: ArgMatches<'_>) {
    if matches.subcommand_matches(INTEGRATION).is_some() {
        integration_run(matches).await;
    } else if matches.subcommand_matches(LOAD_TEST).is_some() {
        load_test_run(matches).await;
    } else if matches.subcommand_matches(DRY_RUN).is_some() {

    } else {

    }
}

async fn integration_run(matches: ArgMatches<'_>) {
    let scenarios = create_scenarios(matches).await;
    let mut error = 0;
    let mut pass = 0;

    for s in scenarios {
        if send(s.scenario, s.path, s.item_op, s.op_code).await == 0 {
            pass += 1;
        } else {
            error += 1;
        }
    }

    if error != 0 {
        println!("{}", format!("There was {} test failure(s)", error.to_string()).red().bold());
        std::process::exit(1)
    } else {
        println!("{}", format!("{} successful test run(s)", pass.to_string()).green().bold());
        std::process::exit(0)
    }
}

async fn load_test_run(matches: ArgMatches<'_>) {
    let users = matches.value_of("users").unwrap_or("10");
    let rate = matches.value_of("rate").unwrap_or("10");
    let time = matches.value_of("time").unwrap_or("30s");
    let url = matches.value_of("url").unwrap_or("localhost:8080");
    let goose_attack = goose::GooseAttack::initialize_with_config(config(usize::from_str(&users).expect("error parsing users"),
                                                                         usize::from_str(&rate).expect("Error parsing rate"), time.to_string(),
                                                                         url.to_string())).expect("Unable to parse load test config");

    let mut req_list = LOAD_TEST_SCENARIOS.write().unwrap();

    req_list.extend(create_scenarios(matches).await);

    std::mem::drop(req_list);

    let goose_stats = goose_attack
        .register_taskset(taskset!("SmokeIT Performance Test")
            .register_task(task!(send_load)))
        .set_default(GooseDefault::RunTime, 1)
        .expect("Something went wrong")
        .execute()
        .await
        .expect("Errors while running performance test");

    goose_stats.print();

}

async fn create_scenarios(matches: ArgMatches<'_>) -> Vec<GooseScenario> {
    let url = matches.value_of("url").unwrap_or("localhost:8080");
    let openapi = parse_openapi(matches.value_of(OPEN_API).expect("Unable to find OpenAPI file!"));
    let test_data = parse_tests(matches.value_of(INPUT).unwrap_or("default.yaml"));
    let context_regex = regex::Regex::new("https://[\\w*|\\.*|\\-*]*").expect("Unable to generate context path regex");
    let context_path = context_regex.replace_all(&openapi.servers.get(rand::thread_rng().gen_range(0..openapi.servers.len())).expect("Unable to retrieve OpenApi server url").url, "");
    let env = matches.value_of("env").unwrap_or("dit");
    let headers = matches.value_of("header");
    let mut global_headers: Option<HashMap<String, String>> = None;

    if let Some(heads) = headers {
        let global = heads.split(",").map(|h| {
            let kv: Vec<&str> = h.split("=").collect();
            if kv.len() == 2 {
                (String::from(kv[0]), String::from(kv[1]))
            } else {
                panic!("Headers must be formatted as <k>=<v>! Value provided was {}", h)
            }
        }).collect::<HashMap<String, String>>();

        if !global.is_empty() {
            global_headers = Some(global);
        }
    }

    for mut data in test_data.data.unwrap_or(Vec::new()) {
        if global_headers.is_some() {
            data.headers.extend(global_headers.clone().unwrap());
        }
        for (op_name, op_impl) in data.operations {
            for mut scenario in op_impl.scenarios {
                println!("{}", format!("--------------- DATA REQUEST '{}' ------------------", scenario.scenario_name).green().bold());
                scenario.sync_global_params(op_impl.params.clone());
                scenario.sync_global_headers(data.headers.clone());
                let (resp, _) = scenario.send(format!("{}{}", url, data.path.clone()), op_name.clone()).await;

                if let Ok(r) = resp {
                    if scenario.response.store {
                        scenario.store_response(r).await;
                    }
                }
                println!("{}", format!("--------------------------------------------").green().bold());
            }
        }
    }

    let mut scenarios = HashMap::new();
    let mut dfs_list = vec![];

    for mut test in test_data.tests {
        if global_headers.is_some() {
            test.headers.extend(global_headers.clone().unwrap());
        }
        let openapi_path: &openapiv3::PathItem = openapi.paths.get(&test.path).expect(&format!("Unable to retrieve path {} from OpenApi", test.path)).to_item_ref();

        for (op_name, op_impl) in test.operations {
            let op_error = &format!("Unable to retrieve {} operation for OpenApi Path {}", op_name, test.path);
            let item_op = match op_name.as_str() {
                "get" => openapi_path.get.as_ref().expect(op_error),
                "post" => openapi_path.post.as_ref().expect(op_error),
                "put" => openapi_path.put.as_ref().expect(op_error),
                "patch" => openapi_path.patch.as_ref().expect(op_error),
                _ => openapi_path.get.as_ref().expect(op_error)
            };

            for mut scenario in op_impl.scenarios {
                scenario.set_env(env);
                scenario.sync_global_params(op_impl.params.clone());
                scenario.sync_global_headers(test.headers.clone());
                dfs_list.push(GooseScenario{scenario : scenario.clone(), path: format!("{}{}{}", url, context_path, test.path.clone()), item_op: item_op.clone(), op_code: op_name.clone()});
                scenarios.insert(scenario.scenario_name.clone(), GooseScenario{scenario, path: format!("{}{}{}", url, context_path, test.path.clone()), item_op: item_op.clone(), op_code: op_name.clone()});
            }
        }
    }
    let mut scenario_list = Vec::with_capacity(scenarios.len());

    fn dependency_find(scenarios: &mut HashMap<String, GooseScenario>, scenario: GooseScenario, scenario_list: &mut Vec<GooseScenario>) {
        if let Some(dependencies) = scenario.scenario.dependencies {
            if dependencies.is_string() {
                if let Some(d) = scenarios.get(dependencies.as_str().unwrap()) {
                    dependency_find(scenarios, d.clone(), scenario_list);
                }
            } else if dependencies.is_sequence() {
                let d_seq = dependencies.as_sequence().unwrap();
                for d in d_seq {
                    if let Some(d) = scenarios.get(&d.as_str().unwrap().to_string()) {
                        dependency_find(scenarios, d.clone(), scenario_list);
                    }
                }
            }
        }
        if let Some(s) = scenarios.remove(&scenario.scenario.scenario_name) {
            scenario_list.push(s);
        }
    }
    for s in dfs_list {
        if scenarios.contains_key(&s.scenario.scenario_name) {
            dependency_find(&mut scenarios, s, &mut scenario_list);
        }
    }
    scenario_list
}

fn dry_run(matches: ArgMatches) {

}

fn parse_openapi(input: &str) -> OpenAPI {
    let openapi: OpenAPI = parse_yaml(input);

    openapi.deref_all()
}

fn parse_tests(input: &str) -> Tests {
    parse_yaml(input)
}

fn parse_yaml<T: DeserializeOwned>(input: &str) -> T {
    let filename = shellexpand::tilde(input).into_owned();
    let file_string = std::fs::read_to_string(filename).expect(&format!("Unable to read file {}", input));
    serde_yaml::from_str(&file_string).expect(&format!("Unable to parse file {} into yaml", input))
}

pub async fn send(mut scenario: crate::model::schema::Scenario, path: String, openapi_op: Operation, op_code: String) -> i32 {
    println!("{}", format!("--------------- Scenario '{}' ------------------", scenario.scenario_name).white().bold()); // todo either description field instead (default to name description doesnt exist), split name if no description?
    let (resp, end) = scenario.send(path, op_code).await;
    let mut return_code = 0;

    match resp {
        Ok(r) => {
            println!("Completed in {} milliseconds", end.to_string());
            println!("Expected acceptance code: {}", scenario.response.status.to_string());

            if scenario.response.status.to_string() == r.status().as_str() {
                println!("{} returned status was {}","SUCCESS:".green().bold(), r.status().as_str());
            } else {
                println!("{} returned status was {}", "FAILURE:".red().bold(), r.status().as_str());
                return_code = 1;
            }

            let op_resp: &openapiv3::Response = openapi_op.responses.responses.get(&openapiv3::StatusCode::Code(r.status().as_u16())).unwrap().to_item_ref();

            if op_resp.content.keys().len() > 0 {
                let expected_content_type: String = op_resp.content.keys().next().unwrap().to_string();
                println!("Content type specified: {}", expected_content_type);
                if expected_content_type == "*/*".to_string() {
                    println!("{} content type in response was {}", "SUCCESS:".green().bold(), r.headers().get("Content-Type").unwrap().to_str().unwrap());
                } else if expected_content_type == r.headers().get("Content-Type").unwrap().to_str().unwrap() {
                    println!("{} content type in response was {}", "SUCCESS:".green().bold(), r.headers().get("Content-Type").unwrap().to_str().unwrap());
                } else {
                    println!("{} content type in response was {}", "FAILURE:".red().bold(), r.headers().get("Content-Type").unwrap().to_str().unwrap());
                    return_code = 1;
                }
            }
            if scenario.response.store {
                scenario.store_response(r).await;
            }
        }

        Err(err) => {
            println!("There was an error completing this scenario");
            println!("{}", err.to_string());
            return_code = 1;
        }
    }

    println!("{}", format!("--------------------------------------------").white().bold());
    return_code
}

use gumdrop::Options;
use reqwest::Response;

fn config(users: usize, request_per_second: usize, time: String, url: String) -> GooseConfiguration {
    let hatch_rate = users * 10;
    let empty_args: Vec<&str> = vec![];

    let mut configuration: GooseConfiguration = GooseConfiguration::parse_args_default(&empty_args).expect("There was an error parsing the defualt goose config");
    configuration.hatch_rate = Some(hatch_rate.to_string());
    configuration.users = Some(users);
    configuration.run_time = time.clone();
    configuration.running_metrics = Some(10);
    configuration.status_codes = true;
    configuration.report_file = "/tmp/smokeIT.html".to_string();
    configuration.debug_format = Some(goose::logger::GooseLogFormat::Json);
    configuration.host = url;
    configuration.throttle_requests = request_per_second;
    configuration.no_task_metrics = true;
    configuration.verbose = 1;

    configuration
}

async fn send_load(user: &mut GooseUser) -> GooseTaskResult {
    let scenario_list = LOAD_TEST_SCENARIOS.read().unwrap().clone();

    for mut goose_scenario in scenario_list {
        let builder = goose_scenario.scenario.build_request(goose_scenario.path, goose_scenario.op_code, false);
        user.goose_send(builder, None).await;
    }

    Ok(())
}