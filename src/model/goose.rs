#[derive(Clone,Debug)]
pub struct GooseScenario {
    pub scenario: crate::model::schema::Scenario,
    pub path: String,
    pub item_op: openapiv3::Operation,
    pub op_code: String
}