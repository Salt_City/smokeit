use std::collections::HashMap;
use std::fmt::Formatter;
use std::iter::FromIterator;
use std::str::FromStr;
use std::sync::TryLockError;
use openapi_utils::{ParameterExt, ReferenceOrExt};
use rand::Rng;
use serde::{de::Visitor, Deserialize, Deserializer};
use serde::de::{Error, MapAccess};
use serde_yaml::Value;
use crate::core::functions::evaluate;

#[derive(Deserialize)]
pub struct Tests {
    pub data: Option<Vec<Schema>>,
    pub tests: Vec<Schema>
}


pub struct Schema {
    pub name: String,
    pub pause: Option<usize>,
    pub path: String,
    pub operations: HashMap<String, Operation>,
    pub headers: HashMap<String, String>
}

impl<'de> Deserialize<'de> for Schema {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        deserializer.deserialize_map(SchemaVisitor)
    }
}

struct SchemaVisitor;

impl<'de> Visitor<'de> for SchemaVisitor {
    type Value = Schema;

    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        write!(formatter, "A schema with one to multiple operations")
    }

    fn visit_map<A>(self, mut map: A) -> Result<Self::Value, A::Error> where A: MapAccess<'de> {
        let mut name: Option<String> = None;
        let mut pause: Option<String> = None;
        let mut path: Option<String> = None;
        let mut body: Option<HashMap<String, Value>> = None;
        let mut op_map: HashMap<String, Operation> = HashMap::new();
        let mut headers: HashMap<String, String> = HashMap::new();
        let mut silent = false;
        let available_ops = "get/post/put/delete/patch";

        while let Some(key) = map.next_key::<String>()? {
            if key == "name".to_string() {
                name = Some(map.next_value()?);
            } else if key == "pause".to_string() {
                pause = Some(map.next_value()?);
            } else if key == "path".to_string() {
                path = Some(map.next_value()?);
            }else if available_ops.contains(&key.to_lowercase()) {
                op_map.insert(key.to_string(), map.next_value()?);
            } else if key == "headers".to_string() {
                headers = map.next_value()?;
            } else if key == "body".to_string() {
                body = Some(map.next_value()?);
            } else if key == "silent".to_string() {
                silent = map.next_value()?;
            } else {
                return Err(Error::custom(&format!("Invalid key: {}", key)));
            }
        }

        if name.is_none() {
            return Err(Error::custom("Missing schema name"));
        } else if name.clone().unwrap().contains(' ') {
            return Err(Error::custom("Names cannot have spaces"))
        }else if path.is_none() {
            return Err(Error::custom("Missing schema path"))
        }

        if op_map.is_empty() {
            let mut op = "get";
            if body.is_some() {
                op = "post";
            }
            op_map.insert(op.to_string(),
                Operation {
                    params: HashMap::new(),
                    body: None,
                    scenarios: vec![Scenario {
                        scenario_name: name.clone().unwrap(),
                        params: HashMap::new(),
                        body,
                        dependencies: None,
                        response: Response::default(),
                        env: String::from("dit"),
                        headers: headers.clone(),
                        silent
                    }]
                });
        }

        Ok(Schema{name: name.unwrap(),
            pause: Some(usize::from_str(&pause.unwrap_or("0".to_string())).unwrap()),
            path: path.unwrap(),
            operations: op_map,
            headers
        })
    }
}

pub struct Operation {
    pub params: HashMap<String, Value>,
    pub body: Option<HashMap<String, Value>>,
    pub scenarios: Vec<Scenario>
}

impl<'de> Deserialize<'de> for Operation {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        deserializer.deserialize_map(OperationVisitor)
    }
}

struct OperationVisitor;

impl<'de> Visitor<'de> for OperationVisitor {
    type Value = Operation;

    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        write!(formatter, "An Operation with one to multiple scenarios")
    }

    fn visit_map<A>(self, mut map: A) -> Result<Self::Value, A::Error> where A: MapAccess<'de> {
        let mut params: HashMap<String, Value> = HashMap::new();
        let mut body: Option<HashMap<String, Value>> = None;
        let mut scenarios: Vec<Scenario> = vec![];

        while let Some(key) = map.next_key::<String>()? {
            if key == "body".to_string() {
                body = Some(map.next_value()?);
            } else if key == "scenarios".to_string() {
                scenarios = map.next_value()?;
            } else {
                params.insert(key, map.next_value()?);
            }
        }

        if scenarios.is_empty() {
            scenarios.push(Scenario{
                scenario_name: "Default".to_string(),
                params: params.clone(),
                body: body.clone(),
                dependencies: None,
                response: Response::default(),
                headers: HashMap::new(),
                env: String::from("dit"),
                silent: false
            })
        }

        Ok(Operation{params, body, scenarios})
    }
}

#[derive(Clone,Debug)]
pub struct Scenario {
    pub scenario_name: String,
    pub params: HashMap<String, Value>,
    pub body: Option<HashMap<String, Value>>,
    pub dependencies: Option<Value>,
    pub response: Response,
    pub headers: HashMap<String, String>,
    pub env: String,
    pub silent: bool
}

impl Scenario {

    pub fn set_env(&mut self, env: &str) {
        self.env = String::from(env);
    }
    pub fn sync_global_headers(&mut self, global_headers: HashMap<String, String>) {
        let mut full_headers = global_headers.iter()
            .filter(|(k,v)| !self.headers.contains_key(&k.to_string()))
            .map(|(k,v)| (k.clone(), v.clone()))
            .collect::<HashMap<String, String>>();

        self.headers.extend(full_headers.clone());
    }

    pub fn sync_global_params(&mut self, global_params: HashMap<String, Value>) {
        let mut full_params = global_params.iter()
            .filter(|(k,v)| !self.params.contains_key(&k.to_string()))
            .map(|(k,v)| (k.clone(), v.clone()))
            .collect::<HashMap<String, Value>>();

        self.params.extend(full_params.clone());
    }

    pub fn build_request(&mut self, base_path: String, op: String, print: bool) -> reqwest::RequestBuilder{
        let endpoint = self.generate_endpoint(base_path);
        let client = reqwest::Client::new();

        // todo I had to do this for all the verbs that can contain a json body because the service would
        //  otherwise return a 415
        if !self.headers.contains_key("Content-Type") && op.as_str() != "get"{
            self.headers.insert("Content-Type".to_string(), "application/json".to_string());
        }

        if print {
            if self.silent {
                println!("*********");
            } else {
                println!("{}", endpoint);
            }
        }

        let mut builder = match op.as_str() {
            "get" => {
                client.get(endpoint)
            }
            "post" => {
                let mut map = self.resolve_body(self.body.as_ref().unwrap().clone());
                let mut body = None;
                if let Some(value) = self.headers.get("Content-Type") {
                    if value == &"application/x-www-form-urlencoded".to_string() {
                        body = Some(serde_urlencoded::to_string(&map).unwrap());
                    }
                }
                client.post(endpoint)
                    .body(body.unwrap_or(serde_json::to_string(&map).unwrap()))
            }
            "put" => {
                let mut map = self.resolve_body(self.body.as_ref().unwrap().clone());
                client.put(endpoint)
                    .body(serde_json::to_string(&map).unwrap())
            }
            "patch" => {
                let mut map = self.resolve_body(self.body.as_ref().unwrap().clone());
                 client.patch(endpoint)
                    .body(serde_json::to_string(&map).unwrap())
            }
            _ => panic!("unable to send request")
        };

        for (hk, hv) in &self.headers {
            builder = builder.header(hk, crate::core::functions::evaluate(hv.chars().collect()));
        }

        builder
    }

    pub async fn send(&mut self, base_path: String, op: String) -> (Result<reqwest::Response, reqwest::Error>, u128) {

        println!("SENDING A {} REQUEST:", op);

        let builder = self.build_request(base_path, op, true);
        let start = std::time::Instant::now();
        let resp = builder.send().await;
        let end = start.elapsed().as_millis();

        (resp, end)
    }

    pub async fn store_response(&self, response: reqwest::Response) {
        match response.status() {
            reqwest::StatusCode::OK |
            reqwest::StatusCode::CREATED |
            reqwest::StatusCode::ACCEPTED => {
                let body = response.text();
                let max_backoff = 2000;
                let mut n = 1;
                loop {
                    match crate::clicommand::RETURN_DATA.try_write() {
                        Ok(mut map) => {
                            let text = &body.await.expect("Error resolving response");
                            let value: Value = serde_json::from_str(text)
                                .unwrap_or(Value::String(text.clone()));
                            map.insert(self.scenario_name.clone(), value);
                            break;
                        }
                        Err(err) => n = Scenario::handle_lock_err(err, max_backoff, n)
                    }
                }
            }
            _ => panic!("Invalid response returned ({}) when attempting to store response", response.status().to_string())
        }
    }

    fn generate_endpoint(&mut self, base_path: String) -> String {
        let mut endpoint = base_path.clone();
        let path_params = self.params.iter()
            .filter(|(k, v)| base_path.contains(&format!("{{{}}}", k)))
            .map(|(k,v)| (k.clone(), v.clone()))
            .collect::<HashMap<String, Value>>();
        let query_params = self.params.iter()
            .filter(|(k,v)| !path_params.contains_key(k.clone()))
            .map(|(k,v)| (k.clone(), v.clone()))
            .collect::<HashMap<String, Value>>();

        for p in self.get_param_value(path_params) {
            endpoint = endpoint.replace(&format!("{{{}}}", p.0), &p.1);
        }
        if query_params.len() > 0 { // todo: use query object from reqwest client?
            endpoint.push('?');
            for q in self.get_param_value(query_params) {
                endpoint = format!("{}{}={}&", endpoint, q.0, q.1);
            }
            endpoint.remove(endpoint.len()-1);
        }
        crate::core::functions::evaluate(endpoint.chars().collect())
    }

    // todo: use the path regex/ maybe be reused on a dry run?
    fn validate_params(&self, item_op: &openapiv3::Operation) -> Vec<(openapiv3::Parameter, Value)> {
        self.params.iter()
            .map(|(k,v)|{
                match item_op.parameters.iter().find(|param| param.to_item_ref().name() == k) {
                    Some(openapi_param) => {
                        (openapi_param.to_item_ref().clone(), v.clone())
                    }
                    None => panic!("Unable to find param {} in OpenApi", k)
                }
            })
            .collect::<Vec<(openapiv3::Parameter, Value)>>()
    }

    fn get_param_value(&self, params: HashMap<String, Value>) -> HashMap<String, String> {
        params.iter()
            .map(|(k,v)| {
                if v.is_mapping() && self.is_env_mapping(v) {
                    Scenario::resolve_value(k.clone(), v.as_mapping().unwrap().get(&Value::String(self.env.clone())).unwrap().clone())
                } else {
                    Scenario::resolve_value(k.clone(), v.clone())
                }
            })
            .collect()
    }

    fn is_env_mapping(&self, v: &Value) -> bool {
        match v.as_mapping() {
            Some(map) => {
                if (map.contains_key(&Value::String(self.env.clone()))) {
                    return true;
                }
                false
            }
            None => false
        }
    }

    pub fn resolve_body(&self, body: HashMap<String, Value>) -> HashMap<String, Value> {
        let mut resolved_body = HashMap::with_capacity(body.len());
        for (k, v) in body {
            if v.is_mapping() && self.is_env_mapping(&v) {
                resolve(k, v.as_mapping().unwrap().get(&Value::String(self.env.clone())).unwrap().clone(), &mut resolved_body);
            } else {
                resolve(k, v, &mut resolved_body);
            }
        }

        fn resolve(k: String, v: Value, resolved_body: &mut HashMap<String, Value>) {
            if v.is_mapping() {
                resolved_body.insert(k, Scenario::resolve_mapping(v));
            } else if v.is_string() {
                resolved_body.insert(k, Value::from(evaluate(v.as_str().unwrap().chars().collect())));
            }
        }
        resolved_body
    }

    pub fn resolve_mapping(mapping: Value) -> Value {
        let map = mapping.as_mapping().unwrap();
        let mut new_map = HashMap::with_capacity(map.len());
        for (k, v) in map {
            if v.is_string() {
                new_map.insert(k.clone(), Value::from(evaluate(v.as_str().unwrap().chars().collect())));
            } else if v.is_mapping() {
                new_map.insert(k.clone(), Scenario::resolve_mapping(v.clone()));
            }
        }
        serde_yaml::to_value(new_map).unwrap()
    }

    pub fn resolve_value(k: String, v: Value) -> (String, String) {
        // todo a sequence always by default will pick a random value
        let value: Value = if v.is_sequence() {
            v.as_sequence().unwrap().get(rand::thread_rng()
                .gen_range(0..v.as_sequence().unwrap().len()))
                .expect(&format!("Unable to get value for param {}", k)).clone()
        } else {
            v
        };
        let string_value: String = match value {
            Value::String(s) => {
                crate::core::functions::evaluate(s.chars().collect())
            },
            Value::Number(n) => n.to_string(),
            Value::Mapping(map) => {
                if k.contains(".") {
                    let (attribute, sub_attributes) = Scenario::consume_attribute(k.clone());
                    Scenario::resolve_value(sub_attributes, map.get(&Value::String(attribute)).expect(&format!("Unable to find mapping for attribute: {}", k.clone())).clone()).1
                } else {
                    Scenario::resolve_value(k.clone(), map.get(&Value::String(k.clone())).expect(&format!("Unable to get value for attribute: {}", k)).clone()).1
                }
            }
            _ => "".to_string()
        };
        (k, string_value)
    }

    pub fn consume_attribute(s: String) -> (String, String) {
        let chars = s.chars().collect::<Vec<char>>();
        let mut chars_iter = chars.iter();
        let mut attribute = "".to_string();

        loop {
            let c = chars_iter.next().expect(&format!("{} is incorrectly formatted", s));
            if c == &'.' {
                break
            } else {
                attribute.push(*c);
            }
        }
        (attribute, String::from_iter(chars_iter))
    }

    pub fn handle_lock_err<T>(err: TryLockError<T>, max_backoff: u64, n: u32) -> u32 {
        match err {
            TryLockError::WouldBlock => {
                Scenario::back_off(n, max_backoff)
            }
            TryLockError::Poisoned(error) => {
                panic!("There was an error attempting to acquire lock \n {}", error.to_string())
            }
        }
    }

    pub fn back_off(n: u32, max_backoff: u64) -> u32 {
        let sleep_time = std::cmp::min(2_u64.pow(n) + rand::thread_rng().gen_range(0..2000), max_backoff);
        std::thread::sleep(std::time::Duration::from_millis(sleep_time));
        n+1
    }
}

impl<'de> Deserialize<'de> for Scenario {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        deserializer.deserialize_map(ScenarioVisitor)
    }
}

struct ScenarioVisitor;

impl<'de> Visitor<'de> for ScenarioVisitor {
    type Value = Scenario;

    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        write!(formatter, "A Test Scenario")
    }

    fn visit_map<A>(self, mut map: A) -> Result<Self::Value, A::Error> where A: MapAccess<'de> {
        let mut name = None;
        let mut params: HashMap<String, Value> = HashMap::new();
        let mut body: Option<HashMap<String, Value>> = None;
        let mut response: Option<Response> = None;
        let mut headers: HashMap<String, String> = HashMap::new();
        let mut dependsOn: Option<Value> = None;
        let mut silent = false;

        while let Some(key) = map.next_key::<String>()? {
            if key == "scenario".to_string() {
                name = Some(map.next_value()?);
            } else if key == "response".to_string() {
                response = Some(map.next_value()?);
            } else if key == "body".to_string() {
                body = Some(map.next_value()?);
            } else if key == "headers".to_string() {
                  headers = map.next_value()?;
            } else if key == "dependsOn".to_string() {
                dependsOn = Some(map.next_value()?);
            } else if key == "silent".to_string() {
                silent = map.next_value()?;
            } else {
                params.insert(key, map.next_value()?);
            }
        }

        if name.is_none() {
            return Err(Error::custom("Scenarios must have a name"))
        }

        Ok(Scenario{
            scenario_name: name.unwrap(),
            params,
            body,
            dependencies: dependsOn,
            response: response.unwrap_or_default(),
            headers,
            env: String::from("dit"),
            silent
        })
    }
}

#[derive(Deserialize,Clone,Debug)]
pub struct Response { // todo: probably needs to be a damn optional for both of these
    pub status: usize,
    pub store: bool
}

impl Default for Response {
    fn default() -> Self {
        Response {
            status: 200, //todo 2xx then use regex?
            store: true
        }
    }
}