#[macro_use]
extern crate clap;
use clap::App;

mod core;
mod clicommand;
mod model;

#[tokio::main]
async fn main() {
    let yaml = load_yaml!("commands.yaml");
    let matches = App::from_yaml(yaml).get_matches();

    clicommand::command(matches).await;
}
