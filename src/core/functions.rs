use serde_yaml::Value;
use crate::model::schema::Scenario;

pub const EMPTY_STRING: &str = "";

// !Ref - returns the value of the specified parameter or resource
//       example: {{ !Ref "some_resource" }}
fn reference(s: Vec<char>) -> String {
    let parameters = evaluate(s).trim().to_string();
    let raw_ref = parameters
        .split("::")
            .map(|s| String::from(s))
            .collect::<Vec<String>>();

    if raw_ref.len() < 1 {
        panic!("Incomplete Ref {}", parameters)
    } else {
        let scenario_name = raw_ref.get(0).unwrap();
        let value = raw_ref.get(1);
        let mut returned_value = None;
        let mut n = 1;
        let max_backoff = 2000;
        let now = std::time::Instant::now();
        loop {
            match crate::clicommand::RETURN_DATA.try_read() {
                Ok(data) => {
                    match data.get(scenario_name) {
                        Some(d) => {
                            if value.is_some() {
                                if d.is_mapping() {
                                    returned_value = if value.unwrap().contains(".") {
                                       Some(
                                           Scenario::resolve_value(value.unwrap().clone(), d.clone())
                                       )
                                    } else {
                                        Some(
                                            Scenario::resolve_value(scenario_name.clone(),
                                                                    d.as_mapping()
                                                                        .unwrap()
                                                                        .get(&Value::String(value.unwrap().into()))
                                                                        .expect(&format!("Value {} does not exist in response for {}", value.unwrap(), scenario_name))
                                                                        .clone())
                                        )
                                    };

                                    break
                                } else {
                                    panic!("Response for {} is not an object, yet an attribute {} was specified", scenario_name, value.unwrap())
                                }
                            } else {
                                returned_value = Some(Scenario::resolve_value(scenario_name.clone(), d.clone()));
                                break
                            }
                        }
                        None => {
                            if now.elapsed().as_secs() > 30 {
                                panic!("Timed out waiting for scenario {} to fill its response", scenario_name)
                            } else {
                                n = crate::model::schema::Scenario::back_off(n, max_backoff)
                            }
                        }
                    }
                }
                Err(err) => n = Scenario::handle_lock_err(err, max_backoff, n)
            }
        }
        returned_value.unwrap().1
    }
}

// !Encrypt - encrypts the string
//       example: {{ !Encrypt !Ref "some_resource" }}
fn encrypt(s: Vec<char>) -> String {
    let parameters = evaluate(s);
    crate::core::encryption::encrypt(parameters.trim().to_string())
}

// !Decrypt - decrypts the string
//       example: {{ !Decrypt !Ref "some_resource" }}
fn decrypt(s: Vec<char>) -> String {
    let parameters = evaluate(s);
    crate::core::encryption::decrypt(parameters.trim().to_string())
}

// !Base64 - returns the Base64 representation of the input string
//       example: {{ !Base64 !Ref "some_resource" }}
fn base64(s: Vec<char>) -> String {
    let parameters = evaluate(s);
    base64_url::encode(parameters.trim())
}

pub fn evaluate(mut s: Vec<char>) -> String {
    let mut templated_string = "".to_string();
    let mut mutilated_string = s.clone();
    if s.len() == 0 {
        "".to_string()
    } else {
        for c in s {
            if c == '$' {
                mutilated_string.remove(0);
                // todo match on `keyword` call to continue walking if a none is returned
                // todo conditional here to continue if it wasn't a function call
                return format!("{}{}", templated_string, keyword(mutilated_string))
            } else {
                mutilated_string.remove(0);
                templated_string.push(c);
            }
        }
        templated_string
    }
}

fn keyword(s: Vec<char>) -> String {
    let (keyword, subset) = get_word(s.clone());

    // todo match on $<function>
    match keyword.as_str() {
        "Ref" => reference(subset),
        "Encrypt" => encrypt(subset),
        "Decrypt" => decrypt(subset),
        "Base64" => base64(subset),
        _ => panic!("Unsupported keyword supplied: {}", keyword)
        // todo recurse back to evaluate
        // todo look into supporting '$'
    }
}

fn get_word(s: Vec<char>) -> (String, Vec<char>) {
    let mut word = String::from(EMPTY_STRING);
    let mut word_counter = 0;

    for c in s.clone() {
        if c == ' ' {
            word_counter += 1;
            break
        } else {
            word.push(c);
            word_counter += 1;
        }
    }

    (word, s[word_counter.. ].to_vec())
}



// Initialize the functions
// pub fn new() -> HashMap<String, Box<dyn Function>> {
//     let mut functions: HashMap<String, Box<dyn Function>> = HashMap::new();
//     functions.insert("!Ref".to_string(), SimpleFunction::new(reference));
//     functions.insert("!Base64".to_string(), SimpleFunction::new(base64));
//     functions.insert("!Encrypt".to_string(), SimpleFunction::new(encrypt));
//     functions.insert("!Decrypt".to_string(), SimpleFunction::new(decrypt));
//     return functions;
// }

#[cfg(test)]
mod tests {
    use std::collections::HashMap;
    use rubble_templates_core::compiler::Compiler;
    use rubble_templates_core::evaluator::Context;
    use rubble_templates_evaluators::simple::compiler::TemplateCompiler;
    use rubble_templates_evaluators::simple::evaluator::SimpleEvaluationEngine;
    use rubble_templates_evaluators::simple::template::Template;
    use crate::core::functions::encrypt;

    // #[test]
    // fn test_encrypt() {
    //     let string = "this_is_a_password";
    //     let encrypted = encrypt(string.chars().collect());
    //     println!("{}", encrypted);
    //     assert_eq!(true, true);
    // }

    // #[test]
    // fn test_base64() {
    //     // parameters
    //     let raw_input = "{{ !Base64 \"test base64\" }}".to_string();
    //
    //     let template = Template::from(raw_input);
    //     let compiler = create_compiler();
    //
    //     assert_eq!(compiler.compile(&template, Context::with_variables(HashMap::new())).unwrap(), "dGVzdCBiYXNlNjQ");
    // }
    //
    // #[test]
    // fn test_no_change() {
    //     let raw = "this is a basic string".to_string();
    //
    //     let template = Template::from(raw);
    //     let compiler = create_compiler();
    //
    //     assert_eq!(compiler.compile(&template, Context::with_variables(HashMap::new())).unwrap(), "this is a basic string");
    // }
    //
    // fn create_compiler()  -> TemplateCompiler<SimpleEvaluationEngine> {
    //     // prepare compilation environment
    //     let engine = SimpleEvaluationEngine::from(crate::core::functions::new());
    //     TemplateCompiler::new(engine)
    // }
}