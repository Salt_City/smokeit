use std::ops::Index;
use std::str;
use std::str::FromStr;

use bytes::Bytes;
use rusoto_core::{HttpClient, Region};
use rusoto_kms::{
    DecryptRequest,
    DecryptResponse,
    EncryptRequest,
    EncryptResponse,
    Kms,
    KmsClient,
};
use async_std::task;
use lazy_static::lazy_static;
use rusoto_core::credential::{ProfileProvider};

const KEY_ID: &str = "alias/mwd-Secret-kms-key";

lazy_static! {
    static ref CLIENT: KmsClient = {
        let arguments: Vec<String> = std::env::args().collect();
        let index = arguments.iter().position(|a| a == "--profile" || a == "-p").unwrap_or_default();
        // todo: stealing, can clean it up later
        let region_index = arguments.iter().position(|a| a == "--region").unwrap_or_default();
        return if index > 0 {
            let provider = ProfileProvider::with_default_credentials(arguments.index(index + 1)).unwrap();
            println!("{} - {}", provider.profile(), provider.region_from_profile().unwrap().unwrap());
            KmsClient::new_with(HttpClient::new().unwrap(), provider.clone(), Region::from_str(provider.region_from_profile().unwrap().unwrap().as_str()).unwrap())
        } else if region_index > 0 {
            KmsClient::new(Region::from_str(arguments.index(region_index + 1)).expect("Unable to parse region supplied!"))
        } else {
            KmsClient::new(Region::default())
        }
    };
}

pub fn decrypt(encrypted_data: String) -> String {
    let request = DecryptRequest {
        grant_tokens: None,
        encryption_context: None,
        encryption_algorithm: None,
        ciphertext_blob: Bytes::from(base64_url::decode(&encrypted_data).unwrap()),
        key_id: Option::from(String::from(KEY_ID))
    };

    let result = task::block_on(CLIENT.decrypt(request));

    match result {
        Ok(response) => parse_decrypt_response(response),
        Err(value) => {
            println!("WARN: Failed to decrypt - {}", value.to_string());
            String::default()
        }
    }
}

pub fn encrypt(plaintext: String) -> String {
    let request = EncryptRequest {
        grant_tokens: None,
        encryption_context: None,
        encryption_algorithm: None,
        key_id: String::from(KEY_ID),
        plaintext: Bytes::from(plaintext)
    };

    let result = task::block_on(CLIENT.encrypt(request));

    match result {
        Ok(response) => parse_encrypt_response(response),
        Err(value) => {
            println!("WARN: Failed to encrypt - {}", value.to_string());
            String::default()
        }
    }
}

fn parse_decrypt_response(response: DecryptResponse) -> String {
    bytes_to_string(response.plaintext).unwrap_or_default()
}

fn parse_encrypt_response(response: EncryptResponse) -> String {
    bytes_to_base64(response.ciphertext_blob).unwrap_or_default()
}

fn bytes_to_string(bytes: Option<Bytes>) -> Option<String> {
    if bytes.is_none() {
        None
    } else {
        Some(String::from_utf8(Vec::from(bytes.unwrap_or_default().as_ref())).unwrap())
    }
}

fn bytes_to_base64(bytes: Option<Bytes>) -> Option<String> {
    if bytes.is_none() {
        None
    } else {
        Some(base64_url::encode(&bytes.unwrap_or_default()))
    }
}
